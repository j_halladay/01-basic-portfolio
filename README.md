Assessment: Basic Portfolio
Overview
In this activity, you will be creating a basic portfolio page. Today, we'll just be focusing on a biography.

Requirements
Create a new folder in your Cloud9 environment and name it as 01-basic-portfolio.

With in that folder create your html file.

For your biography, you have a lot of latitude in what actual content you decide to share. Below are the elements that must be included:

      - a level one heading (h1) giving your biography an appropriate title
  - at least three lower level headings (h2-h6) introducing three paragraphs (p)

  - include a link to your AWS Cloud9 Environment.
Share your environment with 'kenzieacademy' user.

Submit your AWS Cloud9 Environment url in canvas.